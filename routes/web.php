<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


//--------------------Rotas de Acesso ao Index de cada Página----------------------------------//
//Requisições para rotas das páginas de empresa, funcionários e projetos.
Route::get('/empresa', 'EmpresaController@index');
Route::get('/funcionario', 'FuncionarioController@index');
Route::get('/projeto', 'ProjetoController@index');

//-------------------Rotas de Acesso às Views relacionadas aos Funcionarios--------------------//
//Requisições de Views Funcionário
//Requisições para rotas das páginas para registrar novo funcionário
Route::get('/funcionario/novoFuncionario', 'FuncionarioController@novoFuncionario');
Route::post('funcionario/createFuncionario', 'FuncionarioController@createFuncionario');

//Requisições para registrar novas informações no banco de dados
Route::get('/funcionario/{id_funcionario}/atualizarFuncionario', 'FuncionarioController@atualizarFuncionario');
Route::post('/funcionario/{id_funcionario}/storeFuncionario', 'FuncionarioController@updateFuncionario');

//Requisição para abrir informações em detalhe de cada empresa
Route::get('/funcionario/{id_funcionario}/detailedFuncionario', 'FuncionarioController@detailedFuncionario');

//Requisição para remover (visualmente) um funcionário
Route::get('/funcionario/{id_funcionario}/removeFuncionario', 'FuncionarioController@removeFuncionario');

//-------------------Rotas de Acesso às Views relacionadas às Empresas-------------------------//
//Requisição para registrar nova empresa
Route::get('/empresa/novoEmpresa', 'EmpresaController@novoEmpresa');
Route::post('/empresa/createEmpresa', 'EmpresaController@createEmpresa');

//Requisições para registrar novas informações no banco de dados
Route::get('/empresa/{id_empresa}/atualizarEmpresa','EmpresaController@atualizarEmpresa');
Route::post('/empresa/{id_empresa}/storeEmpresa', 'EmpresaController@updateEmpresa');

//Requisição para abrir informações em detalhe de cada empresa
Route::get('/empresa/{id_empresa}/detailedEmpresa', 'EmpresaController@detailedEmpresa');

//Requisição para remover (visualmente) uma empresa
Route::get('/empresa/{id_empresa}/removeEmpresa', 'EmpresaController@removeEmpresa');

//-------------------Rotas de Acesso às Views relacionadas aos Projetos------------------------//
//Requisição para registrar nova empresa
Route::get('/projeto/novoProjeto', 'ProjetoController@novoProjeto');
Route::post('/projeto/createProjeto', 'ProjetoController@createProjeto');

//Requisições para registrar novas informações no banco de dados
Route::get('/projeto/{id_projeto}/atualizarProjeto', 'ProjetoController@atualizarProjeto');
Route::post('/projeto/{id_projeto}/storeProjeto', 'ProjetoController@updateProjeto');

//Requisição para abrir informações em detalhe de cada projeto
Route::get('/projeto/{id_projeto}/detailedProjeto', 'ProjetoController@detailedProjeto');

//Requisição para remover (visualmente) um projeto
Route::get('/empresa/{id_projeto}/removeProjeto', 'EmpresaController@removeProjeto');


//-------------------Rotas de Acesso à Relação Ajax--------------------------------------------//

// Rotas Ajax Projeto
Route::get('/projeto/{id_projeto}/{id_funcionario}/attachProjetoFuncionario', 'ProjetoController@attachProjetoFuncionario');
Route::get('/projeto/{id_projeto}/{id_funcionario}/detachProjetoFuncionario', 'ProjetoController@detachProjetoFuncionario');
