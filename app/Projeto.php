<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Projeto extends Model
{
    //
    protected $fillable = ['name', 'description', 'startDate', 'deadLine', 'endDate'];
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $table = 'projetos';

    public function Empresa()
    {
        return $this->belongsTo(Empresa::class, 'empresa_id');
    }

    public function Funcionarios()
    {
        return $this->belongsToMany(Funcionario::class, 'projetosFuncionarios');
    }

}
