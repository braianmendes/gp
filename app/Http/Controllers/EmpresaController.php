<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Empresa;
use DB;

class EmpresaController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
//------------------------------------------------------------------------------------------------------//
    //Acesso às Views
    //View Funcionário Inicial
    public function index(){
    	$lista_empresa = Empresa::all();

        return view('empresa.index', ['empresas' => $lista_empresa]);
    }
    
    //View Empresa em Detalhe
     public function detailedEmpresa($id_empresa){
        $empresa = Empresa::find($id_empresa);

        return view('empresa.detailed', ['empresa' => $empresa]);
    }

    //View Nova Empresa 
     public function novoEmpresa(){

        return view('empresa.create');
    }

    //View Atualizar Empresa
    public function atualizarEmpresa($id_empresa)
    {
        $empresa = Empresa::find($id_empresa);
        $lista_funcionarios = $empresa->emprFuncionario;
        
        return view('empresa.update',['empresa' => $empresa,
                                    'funcionarios' => $lista_funcionarios
                                    ]);
    }
//-------------------------------------------------------------------------------------------------//
    //Métodos usados para Empresas
    //Método para Criar Nova Empresa
    public function createEmpresa(Request $request){
        $emp = new Empresa();
        $emp->name = $request->name;
        $emp->adress = $request->adress;
        $emp->CNPJ = $request->CNPJ;
        $this->storeEmpresa($emp);

        return redirect('/empresa')->with('message', 'Empresa registrada com sucesso!');
    }

    public function storeEmpresa(Empresa $st){
    $st->save();
    }

    //Métodos para Atualizar Empresa
    public function updateEmpresa(Request $request){
      DB::table('empresas')
                    ->where('id',$request->id)
                    ->update([
                      'name' => $request->name,
                      'adress' => $request->adress,
                      'CNPJ' => $request->CNPJ
                    ]);
        
      return redirect("/empresa")->with("message", 'Empresa atualizada com sucesso!');
    }
//------------------------------------------------------------------------------------------------------//
    //Método para Remover uma Empresa

    public function removeEmpresa($id){
        DB::table('empresas')
                            ->where('id',$id)
                            ->update(['removido' => 1]);
        
        return redirect("/empresa")->with("message", "Empresa removida com sucesso!");
    }

//---------------------------------------------------------------------------------------------------//



//------------------------------------------------------------------------------------------------------//    
}
