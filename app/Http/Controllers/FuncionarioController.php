<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Funcionario;
use App\Empresa;
use DB;

class FuncionarioController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    //--------------------------------------------------------------------------------------------------//
    //Acesso às Views
    //View Funcionário Inicial
    public function index(){
	   	$lista_funcionario = Funcionario::all();
    	return view('funcionario.index',['funcionarios' => $lista_funcionario]);
    }

    //View Funcionario em Detalhe
     public function detailedFuncionario($id_funcionario){
        $funcionario = Funcionario::find($id_funcionario);

        return view('funcionario.detailed', ['funcionario' => $funcionario]);
    }

    //View Novo Funcionário 
     public function novoFuncionario(){

   	return view('funcionario.create');

    }


    //View Atualizar Funcionário
     public function atualizarFuncionario($id_funcionario){
        $funcionario = Funcionario::find($id_funcionario);

        return view('funcionario.update', ['funcionario' => $funcionario]);
    }
    //-------------------------------------------------------------------------------------------------//
    //Métodos usados para Funcionários
    //Método para Salvar Novo Funcionário
    public function createFuncionario(Request $request){
        $fun = new funcionario();
        $fun->name = $request->name;
        $fun->empresa_id = $request->empresa_id;
        $fun->bornYear = $request->bornYear;
        $fun->admissaoYear = $request->admissaoYear;
        $fun->CPF = $request->CPF;
        $fun->RG = $request->RG;

        $fun->save();

        $projetos = $request->projeto_id;
        $fun->projetos()->attach($projetos);

        return redirect('/funcionario')->with('message', 'Funcionário registrado com sucesso!');
    }

    public function storeFuncionario(funcionario $st){
    $st->save();

    }

    public function attachFuncionario(funcionario $st){
    $st->projetos()->attach($projetos);
    
    }

    //Métodos para atualização e registro de novas informações
    public function updateFuncionario(Request $request){
      DB::table('funcionarios')
                    ->where('id',$request->id)
                    ->update([
                      'name' => $request->name,
                      'bornYear' => $request->bornYear
                    ]);
    //attachFuncionario($request->id,$request->projeto_id);

      return redirect("/funcionario")->with("message", 'Funcionário atualizado com sucesso!');
    }

//------------------------------------------------------------------------------------------------------//
    //Método para Remover um Funcionario

    public function removeFuncionario($id){
        DB::table('funcionarios')
                            ->where('id',$id)
                            ->update(['removido' => 1]);
        
        return redirect("/funcionario")->with("message", "Funcionario removido com sucesso!");
    }

//------------------------------------------------------------------------------------------------------//
}