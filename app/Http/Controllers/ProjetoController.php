<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Projeto;
use App\Empresa;
use App\Funcionario;
use DB;

 class ProjetoController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }


//----------------------------------------------------------------------------------------------//
    //Acesso às Views
    //View Projeto Index
    public function index(){
  	   	$lista_projeto = Projeto::all();
    	return view('projeto.index',['projetos' => $lista_projeto]);
    }
    //View Projeto em Detalhe
     public function detailedProjeto($id_projeto){
        $projeto = Projeto::find($id_projeto);

        return view('projeto.detailed', ['projeto' => $projeto]);
    }


    //View Novo Projeto
     public function novoProjeto(){

    	return view('projeto.create');
    }

    //View Atualizar Projeto
     public function atualizarProjeto($id_projeto){
        $projeto = Projeto::find($id_projeto);

        return view('projeto.update', ['projeto' => $projeto]);
    }

    //-------------------------------------------------------------------------------------------------//
    //Métodos usados para Projetos
    //Método para Salvar Novo Projeto
    public function createProjeto(Request $request){
        $pro = new Projeto();
        $pro->name = $request->name;
        $pro->description = $request->description;
        $pro->empresa_id = $request->empresa_id;
        // $pro->funcionario_id = $request->funcionario_id;
        $pro->startDate = $request->startDate;
        $pro->deadLine = $request->deadLine;
        $pro->endDate = $request->endDate;
        $this->storeProjeto($pro);

        return redirect('/projeto')->with('message', 'Projeto registrado com sucesso!');
    }

    public function storeProjeto(Projeto $st){
    $st->save();
    }

    //Métodos para atualização e registro de novas informações
    public function updateProjeto(Request $request){
      DB::table('projetos')
                    ->where('id',$request->id)
                    ->update([
                      'name' => $request->name,
                      'empresa_id' => $request->empresa_id,
                      'startDate' => $request->startDate,
                      'deadLine' => $request->deadLine,
                      'endDate' => $request->endDate
                    ]);
        

      return redirect("/projeto")->with("message", 'Projeto atualizado com sucesso!');
    }

//------------------------------------------------------------------------------------------------------//

    //Método para Remover um Projeto

    public function removeProjeto($id){
        DB::table('projetos')
                            ->where('id',$id)
                            ->update(['removido' => 1]);
        
        return redirect("/projeto")->with("message", "Projeto removido com sucesso!");
    }

//-----------------------------------------------------------------------------------------------------//

    //Método para Ajax do Projeto
    public function attachProjetoFuncionario($id_projeto, $id_funcionario){
        $projeto = Projeto::find($id_projeto);

        $projeto->funcionarios()->attach($id_funcionario);

        $varFuncionario = Funcionario::find($id_funcionario);

        return response()->json([
                'varFuncionario' => $varFuncionario->name
        ]);
    }

    public function detachProjetoFuncionario($id_projeto, $id_funcionario){
        $projeto = Projeto::find($id_projeto);
        $projeto->funcionarios()->detach($id_funcionario);

        $listaFuncionarios = $projeto->Funcionarios;

        return response()->json([
                'listaFuncionarios' => $listaFuncionarios
        ]);
    }

//-----------------------------------------------------------------------------------------------------//    

}
