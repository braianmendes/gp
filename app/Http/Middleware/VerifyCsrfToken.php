<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
    * Handle an incoming request.
    *
    *@param \Illuminate\Http\Request $request
    *@param \Closure $next
    *@return mixed 
    */

    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
    ];

    public function handle($request, Closure $next)
    {
        return parent::handle($request, $next);
    }

    /**
    * Determine if the session and input CSRF tokens match.
    *
    *@param \Illuminate\Http\Request $request
    *@return bool
    */

    protected function tokensMatch($request)
    {
        $token = $request -> ajax() ? $request -> header('X-CSRF-Token') : $request->input('_token');

        return $request->session()->token() == $token;
    }
}
