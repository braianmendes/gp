<?php
//Model da Empresa

namespace App;

use Illuminate\Database\Eloquent\Model;

class Empresa extends Model
{
    protected $fillable = ['name','adress','CNPJ'];
    protected $guarded = ['id', 'created_at', 'update_at'];
    protected $table = 'empresas';

    public function Funcionarios()
    {
    	return $this->hasMany(Funcionario::class, 'empresa_id');
    }
    public function Projetos()
    {
        return $this->hasMany(Projeto::class, 'empresa_id');
    }

}
