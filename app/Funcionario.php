<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Funcionario extends Model
{
    //
    protected $fillable = ['name', 'bornYear', 'admissaoYear', 'CPF', 'RG'];
    protected $guarded = ['id', 'created_at', 'updated_at'];
    protected $table = 'funcionarios';

    public function Empresa()
    {
    	return $this->belongsTo(Empresa::class, 'empresa_id');
    }
    public function Projetos()
    {
        return $this->belongsToMany(Projeto::class, 'projetosFuncionarios');
    }


}
