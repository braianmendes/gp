<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateFuncionariosTabela extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('funcionarios', function (Blueprint $table) {
            $table->Increments('id');
            $table->Integer('removido')->default(0);
            $table->string('name');
            $table->string('bornYear')->nullable();
            $table->string('admissaoYear')->nullable();
            $table->string('CPF')->nullable();
            $table->string('RG')->nullable();                        
            //Referenciando minha tabela Funcionario à tabela Empresa
            $table->Integer('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('id')->on('empresas');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('funcionarios');
    }
}
