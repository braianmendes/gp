<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjetosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projetos', function (Blueprint $table) {
            $table->Increments('id');
            $table->Integer('removido')->default(0);
            $table->string('name');
            $table->string('description')->nullable();            
            $table->string('startDate');
            $table->string('deadLine');
            $table->string('endDate')->nullable();
            //Referenciando minha tabela Projeto à tabela Empresa
            $table->Integer('empresa_id')->unsigned();
            $table->foreign('empresa_id')->references('id')->on('empresas');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projetos');
    }
}
