<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProjetosFuncionariosTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('projetosFuncionarios', function (Blueprint $table) {
            $table->Increments('id');
            $table->timestamps();
            //Referenciando minha tabela Funcionario à tabela Projetos
            $table->Integer('projeto_id')->unsigned();
            $table->foreign('projeto_id')->references('id')->on('projetos');
            //Referenciando minha tabela Projeto à tabela Funcionário
            $table->Integer('funcionario_id')->unsigned();
            $table->foreign('funcionario_id')->references('id')->on('funcionarios');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('projetosFuncionarios');
    }
}
