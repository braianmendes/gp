@extends('layouts.mainlayout')

@section('content')
<div class="container" >
    <div class="row justify-content-center" >
        <div class="col-md-8">

                <center><img src="https://i.ibb.co/80hBjHQ/icon.png" style="width: 250px; height: 250px"></center>

            <div class="card">
                <div class="card-body" style="text-align: left;">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    Bem vindo! Você está logado!</br>
                    </br>
                    Ferramenta Gestor de Projetos permite a você organizar empresas, seus funcionários, e seus respectivos projetos em andamento.</br>
                    </br>
                    Desejamos a você um bom trabalho.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
