@extends('layouts.mainlayout')

@section('content')

<!-- Área de Scripts -->

<!-- Scripts Ajax -->
<!-- Attach Funcionários em Projeto -->
<script type="text/javascript">
function attachProjetoFuncionario(idfunc)
{
	var id_projeto = document.getElementById("ajaxValueProjeto").value;
	var id_funcionario = idfunc;

	$.get('http://127.0.0.1:8000/projeto/'+id_projeto+'/'+id_funcionario+'/attachProjetoFuncionario', function(data){

	var varFuncionario = data.varFuncionario;

	$('#ajaxCardsFuncionarios').append("<div class='card text-white bg-primary' style='min-width: 10rem; display: table-cell; float:left;'><div class='card-header'><a href='http://127.0.0.1:8000/funcionario/"+id_funcionario+"/detailedFuncionario/' style='font-size: 25px'>"+varFuncionario+"</a></strong></div><div class='card-body'><button style='margin-top: 2px; padding: .150rem .75rem' type='button' class='btn btn-primary' onclick=' window.location.href = 'http://127.0.0.1:8000/funcionario/"+id_funcionario+"/atualizarFuncionario';' >Editar</button><div class='modal fade req' tabindex='-1' role='dialog' aria-labelledby='myLargeModalLabel' aria-hidden='true'><div class='modal-dialog'><div class='modal-content'><div class='modal-header'><h5 class='modal-title'>Remover</h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>	<div class='modal-body'><p>Você realmente deseja <strong>REMOVER</strong> o Funcionário ?</p><p class='text-right'><small>*Essa ação não pode ser desfeita</small></p></div><div class='modal-footer'><button type='button' class='btn btn-secondary' data-dismiss='modal'>Cancelar</button><a href='funcionario/"+id_funcionario+"/removeFuncionario/'><button type='button' class='btn btn-danger'>Remover</button></a></div></div></div></div><button style='margin-top: 2px; padding: .150rem .75rem;' type='button' data-toggle='modal' class='btn btn-primary'>Remover</button></div></div>");
	})
}
</script>
<!-- Dettach Funcionários em Projeto -->
<script type="text/javascript">
function detachProjetoFuncionario(idfunc)
{
	var id_projeto = document.getElementById("ajaxValueProjeto").value;
	var id_funcionario = idfunc;

	$.get('http://127.0.0.1:8000/projeto/'+id_projeto+'/'+id_funcionario+'/detachProjetoFuncionario', function(data){
	console.log(data);

	$('#ajaxCardsFuncionarios').empty();

	var listaFuncionarios = data.listaFuncionarios;

	for(i = 0; i < listaFuncionarios.length; i++)
	{
		$('#ajaxCardsFuncionarios').append("<div class='card text-white bg-primary' style='min-width: 10rem; display: table-cell; float:left;'><div class='card-header'><a href='http://127.0.0.1:8000/funcionario/"+listaFuncionarios[i].id+"/detailedFuncionario/' style='font-size: 25px'>"+listaFuncionarios[i].name+"</a></strong></div><div class='card-body'><button style='margin-top: 2px; padding: .150rem .75rem' type='button' class='btn btn-primary' onclick=' window.location.href = 'http://127.0.0.1:8000/funcionario/"+listaFuncionarios[i].id+"/atualizarFuncionario';' >Editar</button><div class='modal fade req' tabindex='-1' role='dialog' aria-labelledby='myLargeModalLabel' aria-hidden='true'><div class='modal-dialog'><div class='modal-content'><div class='modal-header'><h5 class='modal-title'>Remover</h5><button type='button' class='close' data-dismiss='modal' aria-label='Close'><span aria-hidden='true'>&times;</span></button></div>	<div class='modal-body'><p>Você realmente deseja <strong>REMOVER</strong> o Funcionário ?</p><p class='text-right'><small>*Essa ação não pode ser desfeita</small></p></div><div class='modal-footer'><button type='button' class='btn btn-secondary' data-dismiss='modal'>Cancelar</button><a href='funcionario/"+listaFuncionarios[i].id+"/removeFuncionario/'><button type='button' class='btn btn-danger'>Remover</button></a></div></div></div></div><button style='margin-top: 2px; padding: .150rem .75rem;' type='button' data-toggle='modal' class='btn btn-primary'>Remover</button></div></div>");
	}

	})
}
</script>
<!-- Fim da Área de Scripts -->



	<div><center>
	<h1 class="display-3">{{$projeto->name}}</h1>
	</center></div>

	{{ csrf_field() }}

	<div>
			<div class="jumbotron" style="background-color: #32383e">

				<div>

					<h5 style="margin-top: 12px; margin-bottom: 6px; text-align: left;"><strong>Empresa do Projeto: </strong><a href="http://127.0.0.1:8000/projeto/{{ $projeto->empresa->id }}/detailedProjeto/" >{{$projeto->empresa->name}}</a></h5>
							
				</div>

				<div>
					
					<h5 style="margin-top: 12px; margin-bottom: 6px; text-align: left;"><strong>Data de Início: </strong>{{$projeto->startDate}}</h5>
					
					<h5 style="margin-top: 12px; margin-bottom: 6px; text-align: left;"><strong>Entrega Programada: </strong>{{$projeto->deadLine}}</h5>
					
					<h5 style="margin-top: 12px; margin-bottom: 6px; text-align: left;"><strong>Data de Encerramento: </strong>{{$projeto->endDate}}</h5>

				</div>

		<!-- Div Lista de Funcionários -->
		<div>
		<h5 style="margin-top: 12px; margin-bottom: 6px; text-align: left;"><strong>Funcionários Encarregados: </strong></h5>

		<?php
		$funcionarios = $projeto->Funcionarios;
		?>

		<div style="margin-top: 20px; display: table">
			<div id="ajaxCardsFuncionarios" class="mb-6" style="display: table-row; ">
			@foreach($funcionarios as $fun)
				@if($fun->removido != 1)	

				<div class="card text-white bg-primary" style="min-width: 10rem; display: table-cell; float:left;">
					
					<div class="card-header"><a href="http://127.0.0.1:8000/funcionario/{{ $fun->id }}/detailedFuncionario/" style="font-size: 25px">{{$fun->name}}</a></strong></div>

					<div class="card-body">
					<button style="margin-top: 2px; padding: .150rem .75rem" type="button" class="btn btn-primary" onclick=" window.location.href = 'http://127.0.0.1:8000/funcionario/{{$fun->id}}/atualizarFuncionario';" >Editar</button>

					<!-- Span de Garantia de Vontade de Deletar Funcionário-->
					<div class="modal fade req" id="md{{$fun->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">Remover</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	      								<span aria-hidden="true">&times;</span>
	      							</button>
								</div>
								<div class="modal-body">
									<p>Você realmente deseja <strong>REMOVER</strong> o Funcionário {{$fun->name}}?</p>
									<p class="text-right"><small>*Essa ação não pode ser desfeita</small></p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
									<a href="funcionario/{{ $fun->id }}/removeFuncionario/">
										<button type="button" class="btn btn-danger">Remover</button>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- Fim do Span -->

					<!-- Botão de Remover -->
					<button style="margin-top: 2px; padding: .150rem .75rem;" type="button" data-toggle="modal" data-target="#md{{$fun->id}}" class="btn btn-primary">Remover</button>
					<!-- Fim dos Botões de Editar e Remover -->

				</div>
		</div>
		@endif
		@endforeach
	</div>
</div>
</div>
<!-- Fecha Div Lista de Funcionários -->

<!-- Div Escolher Funcionários  -->
<center><div>
	<?php
	$fun = new App\Funcionario;	
	$funcionarios = App\Funcionario::all();
	?>
{{ csrf_field() }}

<div style="margin-top: 20px; float: left;">

	<h4 style="margin-top: 18px">Atualizar Funcionários</h4>
	</br>	
		@foreach($funcionarios as $fun)
			@if($fun->removido != 1)
		<center><div style="display: table-row;">

		<p style="padding-top: 10px; margin-top: 10px; float: left;">{{$fun->name}}</p>
		<input id="ajaxValueFuncionario+{{$fun->id}}" value="{{$fun->id}}" type="hidden" name="">
		
		<div style="float: left;">
		
		<button onclick="attachProjetoFuncionario('{{$fun->id}}')" id="ajaxAdicionarFuncionario"
		class="btn btn-primary" style="margin-left: 10px; padding: 6px; padding-right: 12px; padding-left: 12px; padding-bottom: 10px; ">+</button>
		
		<button onclick="detachProjetoFuncionario('{{$fun->id}}')" id="ajaxRemoverFuncionario" type="button" class="btn btn-primary" style="padding: 6px; padding-right: 16px; padding-left: 16px; padding-bottom: 10px; ">-</button>
		
		</div>
		</div></center>
		@endif
		@endforeach	
	</div>
</div>
<!-- Fim da Div Escolher Funcionários -->

<!-- Div Botões Editar e Remover -->
<center><div style="clear: both;">
				<button style="margin-top: 20px; padding: .150rem .75rem" type="button" class="btn btn-primary" onclick=" window.location.href = 'http://127.0.0.1:8000/projeto/{{$projeto->id}}/atualizarProjeto';" >Editar</button>

				<!-- Span de Garantia de Vontade de Deletar -->
				<div class="modal fade req" id="md{{$projeto->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Remover</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
      								<span aria-hidden="true">&times;</span>
      							</button>
							</div>
							<div class="modal-body">
								<p>Você realmente deseja <strong>REMOVER</strong> o POJETO {{$projeto->name}}?</p>
								<p class="text-right"><small>*Essa ação não pode ser desfeita</small></p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
								<a href="projeto/{{ $projeto->id }}/removeProjeto/">
									<button type="button" class="btn btn-danger">Remover</button>
								</a>
							</div>
						</div>
					</div>
				</div>
				<!-- Fim do Span -->

				<!-- Botão de Remover -->
				<button style="margin-top: 20px; padding: .150rem .75rem" type="button" data-toggle="modal" data-target="#md{{$projeto->id}}" class="btn btn-primary">Remover</button>
</div><center>
<!-- Fecha Div Botões Editar e Remover -->

</div>

</div>
</div>

@endsection