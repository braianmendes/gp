@extends('layouts.mainlayout')

@section('content')
	<center><div>
		<h1 class="display-3">Registro de Novo Projeo</h1>
	</div></center>

	<center><div class="jumbotron" style="background-color: #32383e">

		<form class="col-md-12" action="{{url('/projeto/createProjeto')}}" method="POST">
			{{ csrf_field() }}
			<div class="from-group">
				
				<label class="control-label"></label>
				<?php
				$pro = new App\Projeto;
				$emp = new App\Empresa;
				$emp = App\Empresa::all();
				$fun = new App\Funcionario;
				$fun = App\Funcionario::all();
				?>

				<h5 style="margin-top: 18px">Nome do Projeto</h5>
				<input id="name" name="name" class="form-control" value="{{$pro->name}}" placeholder="Nome">

				<h5 style="margin-top: 18px">Nome da Empresa</h5>
				<select name="empresa_id">
				  <option value=""></option>
				  	<?php
				  	foreach($emp as $key){
				  		echo '<option value="'.$key->id.'">'.$key->name.'</option>';
				  	}
				  	?>
				</select>



				<h5 style="margin-top: 18px">Data de Início</h5>
				<input id="startDate" name="startDate" class="form-control" value="{{$pro->startDate}}" placeholder="Data de Início">

				<h5 style="margin-top: 18px">Entrega Programada</h5>
				<input id="deadLine" name="deadLine" class="form-control" value="{{$pro->deadLine}}" placeholder="Data de Entrega Programada">

				<h5 style="margin-top: 18px">Descrição do Projeto</h5>
				<input id="description" name="description" class="form-control" value="{{$pro->description}}" placeholder="Descrição do Projeto">

				<button style="margin-top: 24px" type="submit" class="btn btn-primary">Salvar</button>

				<button style="margin-top: 24px" type="reset" value="Reset" class="btn btn-primary">Limpar</button>

			</div>

		</form>

		<button style="margin-top: 5px; margin-left: 16px;" onclick="window.location.href = 'http://127.0.0.1:8000/projeto';" class="btn btn-primary">Retornar</button>
	</div></center>

@endsection