@extends('layouts.mainlayout')

@section('content')
	<center><div>
		<h1 class="display-3">Atualização de Projeto</h3>
	</div></h1>

	<center><div  class="jumbotron" style="background-color: #32383e">
		<form class="col-md-12" action="{{url('/projeto/'.$projeto->id.'/storeProjeto')}}" method="POST">
			{{ csrf_field() }}
			<div class="from-group">

				<?php
				$emp = new App\Empresa;
				$emp = App\Empresa::all();
				?>
				
				<label class="control-label"></label>	

				<h5 style="margin-top: 12px">Nome do Projeto</h5>
				<input id="name" name="name" type="text" class="form-control" value="{{$projeto->name}}">

				<h5 style="margin-top: 18px">Nome da Empresa</h5>

				<select name="empresa_id">
				  <option value=""></option>
				  	<?php
				  	foreach($emp as $key){
				  		echo '<option value="'.$key->id.'">'.$key->name.'</option>';
				  	}
				  	?>
				</select>
				
				<!-- <input id="empresa_id" name="empresa_id" type="text" class="form-control" value="{{$projeto->empresa_id}}"> -->

				<!-- <h5 style="margin-top: 18px">Id do Funcionário Encarregado</h5>
				<input id="funcionario_id" name="funcionario_id" class="form-control" value="{{$projeto->funcionario_id}}"> -->

				<h5 style="margin-top: 12px">Data de Início</h5>
				<input id="startDate" name="startDate" type="text" class="form-control" value="{{$projeto->startDate}}" disabled="disabled">

				<h5 style="margin-top: 12px">Entrega Programada</h5>
				<input id="deadLine" name="deadLine" type="text" class="form-control" value="{{$projeto->deadLine}}">

				<h5 style="margin-top: 12px">Data de Encerramento</h5>
				<input id="endDate" name="endDate" type="text" class="form-control" value="{{$projeto->endDate}}">

				<input type="hidden" name="id" value="{{$projeto->id}}">

			<button style="margin-top: 18px" type="submit" class="btn btn-primary">Salvar</button>
			<button style="margin-top: 18px" type="reset" value="Reset" class="btn btn-primary">Limpar</button>
			</div>
			</form>

	</div><center>

	<button style="margin-top: 5px;" onclick="window.location.href = 'http://127.0.0.1:8000/projeto';" class="btn btn-primary">Retornar</button>
@endsection