@extends('layouts.mainlayout')

@section('content')


@if (Session::get('message') == 'Projeto registrado com sucesso!')
<center><div class="alert alert-dismissible alert-light">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
 Projeto registrado com sucesso!
</div></center>
@endif

@if (Session::get('message') == 'Projeto atualizado com sucesso!')
<center><div class="alert alert-dismissible alert-light">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  Funcionário atualizado com sucesso!
</div></center>
@endif

@if (Session::get('message') == 'Projeto removido com sucesso!')
<center><div class="alert alert-dismissible alert-light">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  Funcionario removido com sucesso!
</div></center>
@endif


<center><div>
	<h1>Lista de Projetos</h1>

	<button class="btn btn-primary" onclick="window.location.href = 'http://127.0.0.1:8000/projeto/novoProjeto';" style="margin-top: 10px">Novo Projeto</button>

	<center><div style="margin-top: 20px; display: table">
			<div class="mb-3" style="display: table-row">
		@foreach($projetos as $pro)
			@if($pro->removido != 1)	


			<div class="card text-white bg-primary" style="min-width: 24rem; display: table-cell; float: left">
				<div class="card-header"><a href="projeto/{{ $pro->id }}/detailedProjeto/" style="font-size: 25px">{{$pro->name}}</a></div>
				
				 <div class="card-body">
				<button style="margin-top: 2px; padding: .150rem .75rem" type="button" class="btn btn-primary" onclick=" window.location.href = 'http://127.0.0.1:8000/projeto/{{$pro->id}}/atualizarProjeto';" >Editar</button>

				<!-- Span de Garantia de Vontade de Deletar -->
				<div class="modal fade req" id="md{{$pro->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Remover</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
      								<span aria-hidden="true">&times;</span>
      							</button>
							</div>
							<div class="modal-body">
								<p>Você realmente deseja <strong>REMOVER</strong> o PROJETO {{$pro->name}}?</p>
								<p class="text-right"><small>*Essa ação não pode ser desfeita</small></p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
								<a href="projeto/{{ $pro->id }}/removeProjeto/">
									<button type="button" class="btn btn-danger">Remover</button>
								</a>
							</div>
						</div>
					</div>
				</div>
				<!-- Fim do Span -->

				<!-- Botão de Remover -->
				<button style="margin-top: 2px; padding: .150rem .75rem" type="button" data-toggle="modal" data-target="#md{{$pro->id}}" class="btn btn-primary">Remover</button>
				
				<p class="card-text" style="margin-bottom: 1px"><strong>Nome da Empresa:  </strong>{{$pro->empresa->name}}</p>
				<p class="card-text" style="margin-bottom: 1px"><strong>Data de Início: </strong>{{$pro->startDate}}</p>
				<p class="card-text" style="margin-bottom: 1px"><strong>Entrega Programada: </strong>{{$pro->deadLine}}</p>
				<p class="card-text" style="margin-bottom: 1px"><strong>Data de Encerramento: </strong>{{$pro->endDate}}</p>

				</div>

			</div>
			@endif
		@endforeach
		</div>
	</div></center>
<div></center>

@endsection