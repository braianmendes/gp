@extends('layouts.mainlayout')

@section('content')
	<div>
	<h1 class="display-3">{{$empresa->name}}</h1>
	<div>

	<div>

			<div class="jumbotron" style="background-color: #32383e">

				<div>
					<h5 style="margin-top: 12px; margin-bottom: 6px; text-align: left;"><strong>CNPJ: </strong>{{$empresa->CNPJ}}</h5>

					<h5 style="margin-top: 12px; margin-bottom: 6px; text-align: left;"><strong>Endereço: </strong>{{$empresa->adress}}</h5>
				</div>

		<!-- Div de Divisão de Funcionários -->
		<h3 style="margin-top: 12px; margin-bottom: 6px"><strong>Funcionários: </strong></h3>

		<div>
		<?php
		$funcionarios = $empresa->Funcionarios;
		?>

		<center><div style="margin-top: 20px; display: table">
			<div class="mb-3" style="display: table-row">
			@foreach($funcionarios as $fun)
				@if($fun->removido != 1)	

				<div class="card text-white bg-primary" style="min-width: 23rem; display: table-cell; float: left;">
					
					<div class="card-header"><a href="http://127.0.0.1:8000/funcionario/{{ $fun->id }}/detailedFuncionario/" style="font-size: 25px">{{$fun->name}}</a></strong></div>

					<div class="card-body">
					<button style="margin-top: 2px; padding: .150rem .75rem" type="button" class="btn btn-primary" onclick=" window.location.href = 'http://127.0.0.1:8000/funcionario/{{$fun->id}}/atualizarFuncionario';" >Editar</button>

					<!-- Span de Garantia de Vontade de Deletar Funcionário-->
					<div class="modal fade req" id="md{{$fun->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">Remover</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	      								<span aria-hidden="true">&times;</span>
	      							</button>
								</div>
								<div class="modal-body">
									<p>Você realmente deseja <strong>REMOVER</strong> o Funcionário {{$fun->name}}?</p>
									<p class="text-right"><small>*Essa ação não pode ser desfeita</small></p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
									<a href="funcionario/{{ $fun->id }}/removeFuncionario/">
										<button type="button" class="btn btn-danger">Remover</button>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- Fim do Span -->

					<!-- Botão de Remover -->
					<button style="margin-top: 2px; padding: .150rem .75rem" type="button" data-toggle="modal" data-target="#md{{$fun->id}}" class="btn btn-primary">Remover</button>
					<!-- Fim dos Botões de Editar e Remover -->

					<p class="card-text" style="margin-bottom: 1px"><strong>Ano de Nascimento: </strong>{{$fun->bornYear}}</p>
					<p class="card-text" style="margin-bottom: 1px"><strong>Data de Admissão: </strong>{{$fun->admissaoYear}}</p>				
					<p class="card-text" style="margin-bottom: 1px"><strong>CPF: </strong>{{$fun->CPF}}</p>

					<p class="card-text" style="margin-bottom: 1px"><strong>RG: </strong>{{$fun->RG}}</p>
					</div>
				</div>
				@endif
			@endforeach
		</div>
	</div></center>

		<h3 style="margin-top: 12px; margin-bottom: 6px"><strong>Projetos: </strong></h3>

		<center><div style="margin-top: 20px; display: table; clear: both">
					<div class="mb-3" style="display: table-row">

				<?php
				$projetos = $empresa->Projetos;
				?>

				@foreach($projetos as $pro)
					@if($pro->removido != 1)	


				<div class="card text-white bg-primary" style="min-width: 23rem; display: table-cell; float: left;">
					<div class="card-header"><a href="http://127.0.0.1:8000/projeto/{{ $pro->id }}/detailedProjeto/" style="font-size: 25px">{{$pro->name}}</a></div>
					
					 <div class="card-body">
					<button style="margin-top: 2px; padding: .150rem .75rem" type="button" class="btn btn-primary" onclick=" window.location.href = 'http://127.0.0.1:8000/projeto/{{$pro->id}}/atualizarProjeto';" >Editar</button>


					<!-- Span de Garantia de Vontade de Deletar Funcionário-->
					<div class="modal fade req" id="md{{$pro->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">Remover</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	      								<span aria-hidden="true">&times;</span>
	      							</button>
								</div>
								<div class="modal-body">
									<p>Você realmente deseja <strong>REMOVER</strong> o Projeto {{$pro->name}}?</p>
									<p class="text-right"><small>*Essa ação não pode ser desfeita</small></p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
									<a href="projeto/{{ $pro->id }}/removeProjeto/">
										<button type="button" class="btn btn-danger">Remover</button>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- Fim do Span -->

					<!-- Botão de Remover -->
					<button style="margin-top: 2px; padding: .150rem .75rem" type="button" data-toggle="modal" data-target="#md{{$pro->id}}" class="btn btn-primary">Remover</button>
					<!-- Fim dos Botões de Editar e Remover -->

					<p class="card-text" style="margin-bottom: 1px"><strong>Data de Início: </strong>{{$pro->startDate}}</p>
					<p class="card-text" style="margin-bottom: 1px"><strong>Entrega Programada: </strong>{{$pro->deadLine}}</p>
					<p class="card-text" style="margin-bottom: 1px"><strong>Data de Encerramento: </strong>{{$pro->endDate}}</p>
					</div>

				</div>
				@endif
			@endforeach
			</div>
		</div></center>

			    <div style="clear: both;">
					<button style="margin-top: 40px; padding: .150rem .75rem" type="button" class="btn btn-primary" onclick=" window.location.href = 'http://127.0.0.1:8000/empresa/{{$empresa->id}}/atualizarEmpresa';" >Editar</button>

				<!-- Span de Garantia de Vontade de Deletar -->
				<div class="modal fade req" id="md{{$empresa->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Remover</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
      								<span aria-hidden="true">&times;</span>
      							</button>
							</div>
							<div class="modal-body">
								<p>Você realmente deseja <strong>REMOVER</strong> a Empresa {{$empresa->name}}?</p>
								<p class="text-right"><small>*Essa ação não pode ser desfeita</small></p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
								<a href="empresa/{{ $empresa->id }}/removeEmpresa/">
									<button type="button" class="btn btn-danger">Remover</button>
								</a>
							</div>
						</div>
					</div>
				</div>
				<!-- Fim do Span -->

				<!-- Botão de Remover -->
				<button style="margin-top: 40px; padding: .150rem .75rem" type="button" data-toggle="modal" data-target="#md{{$empresa->id}}" class="btn btn-primary">Remover</button>

				</div>

			</div>


		<button style="margin-top: 5px;" onclick="window.location.href = 'http://127.0.0.1:8000/empresa';" class="btn btn-primary">Retornar</button>
	</div>

@endsection