@extends('layouts.mainlayout')

@section('content')
<!-- Área de Scripts -->

<!-- Scripts Ajax -->
<!-- Attach Funcionários em Empresa -->
<script type="text/javascript">
function attachEmpresaFuncionario(idfunc)
{
	var id_empresa = document.getElementById("ajaxValueEmpresa").value;
	var id_funcionario = idfunc;

	$.get('http://127.0.0.1:8000/empresa/'+id_empresa+'/'+id_funcionario+'/attachEmpresaFuncionario', function(data){

	var varFuncionario = data.varFuncionario;

	$('#ajaxPFuncionarios').append("<li style='text-align: left; font-size: 20px'>"+varFuncionario+"</li>");
	})
}
</script>
<!-- Dettach Funcionários em Empresa -->
<script type="text/javascript">
function detachEmpresaFuncionario(idfunc)
{
	var id_empresa = document.getElementById("ajaxValueEmpresa").value;
	var id_funcionario = idfunc;

	$.get('http://127.0.0.1:8000/empresa/'+id_empresa+'/'+id_funcionario+'/detachEmpresaFuncionario', function(data){
	console.log(data);

	$('#ajaxPFuncionarios').empty();

	var listaFuncionarios = data.listaFuncionarios;

	for(i = 0; i < listaFuncionarios.length; i++)
	{
	$('#ajaxPFuncionarios').append("<li style='text-align: left; font-size: 20px'>"+varFuncionario+"</li>");
	}

	})
}
</script>
<!-- Fim da Área de Scripts -->




	<?php
		$emp = new App\Empresa;
		$emp = App\Empresa::all();
	?>

	<center><div>
	<h1 class="display-3">{{$empresa->name}}</h1>
	</div></center>

	<center>
	<div class="jumbotron" style="background-color: #32383e">

		<form class="col-md-12" action="{{url('empresa/'.$empresa->id.'/storeEmpresa')}}" method="POST">
			{{ csrf_field() }}

			<div class="from-group" >
				
				<label class="control-label"></label>
				
				<h5 style="margin-top: 12px; margin-bottom: 6px">Nome da Empresa</h5>
				<input name="name" class="form-control" value="{{$empresa->name}}">

				<h5 style="margin-top: 12px; margin-bottom: 6px">CNPJ</h5>
				<input name="CNPJ" class="form-control" readonly="readonly" value="{{$empresa->CNPJ}}">

				<h5 style="margin-top: 12px; margin-bottom: 6px">Endereço</h5>
				<input name="adress" class="form-control" value="{{$empresa->adress}}">

				<input type="hidden" name="id" value="{{$empresa->id}}">

				<center></center><div style="clear: both">
				<button style="margin-top: 12px; margin-bottom: 6px" class="btn btn-primary">Salvar</button>

				<button style="margin-top: 12px; margin-bottom: 6px" type="reset" value="Reset" class="btn btn-primary">Limpar</button>
				</div>

			</div>

		</form>

</div></center>

		<button style="margin-top: 5px;" onclick="window.location.href = 'http://127.0.0.1:8000/empresa';" class="btn btn-primary">Retornar</button>
	</div></center>

@endsection