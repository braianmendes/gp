@extends('layouts.mainlayout')

@section('content')
	<center><div>
		<h1 class="display-3">Registro de Nova Empresa</h1>
	</div></center>

	<center><div class="jumbotron" style="background-color: #32383e">

		<form class="col-md-12" action="{{url('/empresa/createEmpresa')}}" method="POST">
			{{ csrf_field() }}
			<div class="from-group">
				
				<label class="control-label"></label>
				<?php
				$token = mt_rand();

				$emp = new App\Empresa;
				$emp->empresa_id = $token;

				/*
				<h5>Nome da Empresa</h5>
				<select name="empresa_id" !important>
					@foreach($emp as $key)
					  <option value="{{$key->id}}">{{$key->name}}</option>
					@endforeach
				</select>*/
				?>

				<input type="hidden" id="empresa_id" name="empresa_id" class="form-control" value="{{$emp->empresa_id}}">


				<h5 style="margin-top: 18px">Nome da Empresa</h5>
				<input id="name" name="name" class="form-control" value="{{$emp->name}}" placeholder="Nome">

				<h5 style="margin-top: 18px">Endereço</h5>
				<input id="adress" name="adress" class="form-control" value="{{$emp->adress}}" placeholder="Endereço">

				<h5 style="margin-top: 18px">CNPJ</h5>
				<input id="CNPJ" name="CNPJ" class="form-control" value="{{$emp->CNPJ}}" placeholder="CNPJ">

				<button style="margin-top: 24px" type="submit" class="btn btn-primary">Salvar</button>

				<button style="margin-top: 24px" type="reset" value="Reset" class="btn btn-primary">Limpar</button>

			</div>

		</form>

		<button style="margin-top: 5px; margin-left: 16px;" onclick="window.location.href = 'http://127.0.0.1:8000/empresa';" class="btn btn-primary">Retornar</button>
	</div></center>

@endsection