@extends('layouts.mainlayout')

@section('content')



@if (Session::get('message') == 'Empresa registrada com sucesso!')
<center><div class="alert alert-dismissible alert-light">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  Empresa registrada com sucesso!
</div></center>
@endif

@if (Session::get('message') == 'Empresa atualizada com sucesso!')
<center><div class="alert alert-dismissible alert-light">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  Empresa atualizada com sucesso!
</div></center>
@endif

@if (Session::get('message') == 'Empresa removida com sucesso!')
<center><div class="alert alert-dismissible alert-light">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  Empresa removida com sucesso!
</div></center>
@endif



<center><div>
	<h1>Lista de Empresas</h1>

	<button class="btn btn-primary" onclick="window.location.href = 'http://127.0.0.1:8000/empresa/novoEmpresa';"  style="margin-top: 10px">Nova Empresa</button>

	<center><div style="margin-top: 20px; display: table">
			<div class="mb-3" style="display: table-row">
		@foreach($empresas as $em)
			@if($em->removido != 1)	

			<!-- Botões de Editar e Remover -->
			<!-- Botão de Editar Empresa -->
			<div class="card text-white bg-primary" style="min-width: 24rem; display: table-cell; float: left">
				<div class="card-header"><a href="empresa/{{ $em->id }}/detailedEmpresa/" style="font-size: 25px">{{$em->name}}</a></div>
				
				 <div class="card-body">
				<button style="margin-top: 2px; padding: .150rem .75rem" type="button" class="btn btn-primary" onclick=" window.location.href = 'http://127.0.0.1:8000/empresa/{{$em->id}}/atualizarEmpresa';" >Editar</button>


				<!-- Span de Garantia de Vontade de Deletar -->
				<div class="modal fade req" id="md{{$em->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Remover</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
      								<span aria-hidden="true">&times;</span>
      							</button>
							</div>
							<div class="modal-body">
								<p>Você realmente deseja <strong>REMOVER</strong> a Empresa {{$em->name}}?</p>
								<p class="text-right"><small>*Essa ação não pode ser desfeita</small></p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
								<a href="empresa/{{ $em->id }}/removeEmpresa/">
									<button type="button" class="btn btn-danger">Remover</button>
								</a>
							</div>
						</div>
					</div>
				</div>
				<!-- Fim do Span -->

				<!-- Botão de Remover -->
				<button style="margin-top: 2px; padding: .150rem .75rem" type="button" data-toggle="modal" data-target="#md{{$em->id}}" class="btn btn-primary">Remover</button>

				<p class="card-text" style="margin-bottom: 1px; margin-top: 10px"><strong>CNPJ:  </strong>{{$em->CNPJ}}</p>
				<p class="card-text" style="margin-bottom: 1px"><strong>Endereço:  </strong>{{$em->adress}}</p>
				</div>
				<!-- Fim dos Botões de Editar e Remover -->

			</div>
			@endif
		@endforeach
		</div>
	</div></center>
<div></center>
@endsection