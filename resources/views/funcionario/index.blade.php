@extends('layouts.mainlayout')

@section('content')


@if (Session::get('message') == 'Funcionário registrado com sucesso!')
<center><div class="alert alert-dismissible alert-light">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
 Funcionário registrado com sucesso!
</div></center>
@endif

@if (Session::get('message') == 'Funcionário atualizado com sucesso!')
<center><div class="alert alert-dismissible alert-light">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  Funcionário atualizado com sucesso!
</div></center>
@endif

@if (Session::get('message') == 'Funcionario removido com sucesso!')
<center><div class="alert alert-dismissible alert-light">
  <button type="button" class="close" data-dismiss="alert">&times;</button>
  Funcionario removido com sucesso!
</div></center>
@endif


<div>
	<h1>Lista de Funcionários</h1>

	<button class="btn btn-primary" onclick="window.location.href = 'http://127.0.0.1:8000/funcionario/novoFuncionario';"  style="margin-top: 10px">Novo Funcionário</button>

		<?php
		$funcionario = new App\Funcionario;
		?>

	<center><div style="margin-top: 20px; display: table">
			<div class="mb-3" style="display: table-row">
	
		@foreach($funcionarios as $fun)
			@if($fun->removido != 1)
			<div class="card text-white bg-primary" style="min-width: 24rem; display: table-cell; float: left;" >
				
				<div class="card-header"><a href="funcionario/{{ $fun->id }}/detailedFuncionario/" style="font-size: 25px">{{$fun->name}}</a></strong></div>

				<div class="card-body">
				<button style="margin-top: 2px; padding: .150rem .75rem" type="button" class="btn btn-primary" onclick=" window.location.href = 'http://127.0.0.1:8000/funcionario/{{$fun->id}}/atualizarFuncionario';" >Editar</button>

				<!-- Span de Garantia de Vontade de Deletar -->
				<div class="modal fade req" id="md{{$fun->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Remover</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
      								<span aria-hidden="true">&times;</span>
      							</button>
							</div>
							<div class="modal-body">
								<p>Você realmente deseja <strong>REMOVER</strong> a Empresa {{$fun->name}}?</p>
								<p class="text-right"><small>*Essa ação não pode ser desfeita</small></p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
								<a href="funcionario/{{ $fun->id }}/removeFuncionario/">
									<button type="button" class="btn btn-danger">Remover</button>
								</a>
							</div>
						</div>
					</div>
				</div>
				<!-- Fim do Span -->

				<!-- Botão de Remover -->
				<button style="margin-top: 2px; padding: .150rem .75rem" type="button" data-toggle="modal" data-target="#md{{$fun->id}}" class="btn btn-primary">Remover</button>
				<!-- Fim dos Botões de Editar e Remover -->

				<p class="card-text" style="margin-bottom: 1px"><strong>Nome da Empresa: </strong>{{$fun->empresa->name}}</p>
				<p class="card-text" style="margin-bottom: 1px"><strong>Ano de Nascimento: </strong>{{$fun->bornYear}}</p>
				<p class="card-text" style="margin-bottom: 1px"><strong>Ano de Admissão: </strong>{{$fun->admissaoYear}}</p>
				<p class="card-text" style="margin-bottom: 1px"><strong>CPF: </strong>{{$fun->CPF}}</p>
				<p class="card-text" style="margin-bottom: 1px"><strong>RG: </strong>{{$fun->RG}}</p>

				</div>

			</div>
		@endif
		@endforeach

		</div>
	</div></center>
<div>

@endsection