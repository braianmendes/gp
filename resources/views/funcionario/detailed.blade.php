@extends('layouts.mainlayout')

@section('content')

	<?php
		$fun = new App\Funcionario;
		$fun = App\Funcionario::all();
	?>

	<h1  class="display-3">{{$funcionario->name}}</h1>

	<div>

			<div class="jumbotron" style="background-color: #32383e">

				<?php
					$nomeDaEmpresa = $funcionario->empresa;
				?>

				<h5 style="margin-top: 12px; margin-bottom: 6px; text-align: left;"><strong>Ano de Nascimento: </strong>{{$funcionario->bornYear}}</h5>

				<h5 style="margin-top: 12px; margin-bottom: 6px; text-align: left;"><strong>Idade: </strong>{{ 2019 - $funcionario->bornYear}}</h5>

				<h5 style="margin-top: 12px; margin-bottom: 6px; text-align: left;"><strong>Trabalha na Empresa: </strong><a href="http://127.0.0.1:8000/empresa/{{ $funcionario->empresa->id }}/detailedEmpresa/" >{{$funcionario->empresa->name}}</a></h5>

				<h5 style="margin-top: 12px; margin-bottom: 6px; text-align: left;"><strong>Projetos: </strong></h5>

<div style="margin-top: 20px; display: table">
			<div class="mb-3" style="display: table-row">

				<?php
				$projetos = $funcionario->Projetos;
				?>

				@foreach($projetos as $pro)
					@if($pro->removido != 1)

				<div class="card text-white bg-primary" style="min-width: 24rem; display: table-cell; float: left;">
					<div class="card-header"><a href="http://127.0.0.1:8000/projeto/{{ $pro->id }}/detailedProjeto/" style="font-size: 25px">{{$pro->name}}</a></div>
					
					 <div class="card-body">
					<button style="margin-top: 2px; padding: .150rem .75rem" type="button" class="btn btn-primary" onclick=" window.location.href = 'http://127.0.0.1:8000/projeto/{{$pro->id}}/atualizarProjeto';" >Editar</button>


					<!-- Span de Garantia de Vontade de Deletar Funcionário-->
					<div class="modal fade req" id="md{{$pro->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
						<div class="modal-dialog">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title">Remover</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
	      								<span aria-hidden="true">&times;</span>
	      							</button>
								</div>
								<div class="modal-body">
									<p>Você realmente deseja <strong>REMOVER</strong> o Projeto {{$pro->name}}?</p>
									<p class="text-right"><small>*Essa ação não pode ser desfeita</small></p>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
									<a href="projeto/{{ $pro->id }}/removeProjeto/">
										<button type="button" class="btn btn-danger">Remover</button>
									</a>
								</div>
							</div>
						</div>
					</div>
					<!-- Fim do Span -->

					<!-- Botão de Remover -->
					<button style="margin-top: 2px; padding: .150rem .75rem" type="button" data-toggle="modal" data-target="#md{{$pro->id}}" class="btn btn-primary">Remover</button>
					<!-- Fim dos Botões de Editar e Remover -->

					<p class="card-text" style="margin-bottom: 1px"><strong>Data de Início: </strong>{{$pro->startDate}}</p>
					<p class="card-text" style="margin-bottom: 1px"><strong>Entrega Programada: </strong>{{$pro->deadLine}}</p>
					<p class="card-text" style="margin-bottom: 1px"><strong>Data de Encerramento: </strong>{{$pro->endDate}}</p>
					</div>

				</div>
				@endif
			@endforeach
			</div>
		</div>
</div></div>


				<button style="margin-top: 40px; padding: .150rem .75rem" type="button" class="btn btn-primary" onclick=" window.location.href = 'http://127.0.0.1:8000/funcionario/{{$funcionario->id}}/atualizarFuncionario';" >Editar</button>

				<!-- Span de Garantia de Vontade de Deletar -->
				<div class="modal fade req" id="md{{$funcionario->id}}" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
					<div class="modal-dialog">
						<div class="modal-content">
							<div class="modal-header">
								<h5 class="modal-title">Remover</h5>
								<button type="button" class="close" data-dismiss="modal" aria-label="Close">
      								<span aria-hidden="true">&times;</span>
      							</button>
							</div>
							<div class="modal-body">
								<p>Você realmente deseja <strong>REMOVER</strong> a Empresa {{$funcionario->name}}?</p>
								<p class="text-right"><small>*Essa ação não pode ser desfeita</small></p>
							</div>
							<div class="modal-footer">
								<button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
								<a href="funcionario/{{ $funcionario->id }}/removeFuncionario/">
									<button type="button" class="btn btn-danger">Remover</button>
								</a>
							</div>
						</div>
					</div>
				</div>
				<!-- Fim do Span -->

				<!-- Botão de Remover -->
				<button style="margin-top: 40px; padding: .150rem .75rem" type="button" data-toggle="modal" data-target="#md{{$funcionario->id}}" class="btn btn-primary">Remover</button>
				<!-- Fim dos Botões de Editar e Remover -->

			</div>


		<button style="margin-top: 5px; margin-left: 16px;" onclick="window.location.href = 'http://127.0.0.1:8000/funcionario';" class="btn btn-primary">Retornar</button>
	</div>

@endsection