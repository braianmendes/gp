@extends('layouts.mainlayout')

@section('content')
	<center><div>
		<h1 class="display-3">Registro de Novo Funcionário</h1>
	</div></center>

	<center><div class="jumbotron" style="background-color: #32383e">

		<form class="col-md-12" action="{{url('/funcionario/createFuncionario')}}" method="POST">
			{{ csrf_field() }}
			<div class="from-group">
				
				<label class="control-label"></label>
				<?php
				$fun = new App\Funcionario;
				$emp = new App\Empresa;
				$emp = App\Empresa::all();
				$pro = new App\Projeto;
				$pro = App\Projeto::all();
				?>

				<h5 style="margin-top: 18px">Nome do Funcionário</h5>
				<input id="name" name="name" class="form-control" value="{{$fun->name}}" placeholder="Nome">

				<h5 style="margin-top: 18px">Nome da Empresa</h5>

				<select name="empresa_id">
				  <option value=""></option>
				  	<?php
				  	foreach($emp as $key){
				  		echo '<option value="'.$key->id.'">'.$key->name.'</option>';
				  	}
				  	?>
				</select>

				<h5 style="margin-top: 18px">Projeto Inicial</h5>

				<select name="projeto_id">
				  <option value=""></option>
				  	<?php
				  	foreach($pro as $key){
				  		echo '<option value="'.$key->id.'">'.$key->name.'</option>';
				  	}
				  	?>
				</select>


				<h5 style="margin-top: 18px">Data de Nascimento</h5>
				<input id="bornYear" name="bornYear" class="form-control" value="{{$fun->bornYear}}" placeholder="Ano de Nascimento">

				<h5 style="margin-top: 18px">Data de Admissão</h5>
				<input id="admissaoYear" name="admissaoYear" class="form-control" value="{{$fun->admissaoYear}}" placeholder="Ano de Admissão">

				<h5 style="margin-top: 18px">CPF</h5>
				<input id="CPF" name="CPF" class="form-control" value="{{$fun->CPF}}" placeholder="CPF">

				<h5 style="margin-top: 18px">RG</h5>
				<input id="RG" name="RG" class="form-control" value="{{$fun->RG}}" placeholder="RG">


				<button style="margin-top: 24px" type="submit" class="btn btn-primary">Salvar</button>

				<button style="margin-top: 24px" type="reset" value="Reset" class="btn btn-primary">Limpar</button>

			</div>

		</form>

		<button style="margin-top: 5px; margin-left: 16px;" onclick="window.location.href = 'http://127.0.0.1:8000/funcionario';" class="btn btn-primary">Retornar</button>
	</div></center>

@endsection