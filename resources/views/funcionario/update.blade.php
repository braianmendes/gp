@extends('layouts.mainlayout')

@section('content')
	<center><div>
		<h1 class="display-3">Atualização de Funcionário</h3>
	</div></h1>

	<center><div class="jumbotron" style="background-color: #32383e">
		<form class="col-md-12" action="{{url('/funcionario/'.$funcionario->id.'/storeFuncionario')}}" method="POST">
			{{ csrf_field() }}
			<div class="from-group">
				
				<label class="control-label"></label>

				<h5 style="margin-top: 12px">Nome do Funcionário</h5>
				<input id="name" name="name" type="text" class="form-control" value="{{$funcionario->name}}">

	  				<?php
	  				$empresa = new App\Empresa;
	  				$empresa = App\Empresa::all();
					$pro = new App\Projeto;
					$pro = App\Projeto::all();
	  				?>


				<h5 style="margin-top: 18px">Responsável pelos Projetos</h5>

				<select name="projeto_id" multiple >
				<?php
				foreach($pro as $key){
					echo '</br><option value="'.$key->id.'">'.$key->name.'</option></br>';	
				}
				?>
				</select>

				<h5 style="margin-top: 12px">Data de Nascimento</h5>
				<input id="bornYear" name="bornYear" type="text" class="form-control" value="{{$funcionario->bornYear}}">

				<h5 style="margin-top: 12px">Data de Admissão</h5>
				<input id="admissaoYear" name="admissaoYear" type="text" class="form-control" value="{{$funcionario->admissaoYear}}">

				<h5 style="margin-top: 12px">CPF</h5>
				<input id="CPF" name="CPF" type="text" class="form-control" value="{{$funcionario->CPF}}">

				<h5 style="margin-top: 12px">RG</h5>
				<input id="RG" name="RG" type="text" class="form-control" value="{{$funcionario->RG}}">

				<input type="hidden" name="id" value="{{$funcionario->id}}">

			<button style="margin-top: 18px" type="submit" class="btn btn-primary">Salvar</button>
			<button style="margin-top: 18px" type="reset" value="Reset" class="btn btn-primary">Limpar</button>
			</div>
			</form>
	</div><center>

	<button style="margin-top: 5px;" onclick="window.location.href = 'http://127.0.0.1:8000/funcionario';" class="btn btn-primary">Retornar</button>
@endsection